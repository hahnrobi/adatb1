create table auto (
    rendszam char(6) primary key,
    marka varchar(30) not null,
    modell varchar(15),
    szin varchar(15),
    evjarat year(4),
    uzemenyag varchar(10),
    ar int unsigned);

desc auto;

alter table auto add ulesek_szama tinyint(2) unsigned;
alter table auto add allapot varchar(25);

alter table auto modify modell varchar(20);

insert into auto values(
    'ABC123', 'Volkswagen', 'Passat', 'szürke', 2005, 'diesel', 2200000,5, 'új');

insert into auto values(
    'NNN121', 'Ford', 'Mustang', 'vörös', 2015, 'benzin', 5500000,2, 'új');

insert into auto values(
    'BPM120', 'BMW', 'X5', 'fekete', 2008, 'diesel', 3680000,5, 'használt');

insert into auto values(
    'RKE312', 'Lada', '1200', 'kék', 1985, 'benzin', 250000,5,'széthajtott');

insert into auto values(
    'MAE573', 'Mazda', 'MX5', 'vörös', 2013, 'benzin', 3410000,2, 'új');

update auto set modell = 'Normál' where ar > 5000000;

rename table auto to Gepjarmu;

select * from Gepjarmu;

update Gepjarmu set ar = (ar*1.1) where ar < 600000;

delete from Gepjarmu where marka = 'Skoda';
