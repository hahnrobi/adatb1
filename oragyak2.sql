#a
create table beteg (
tajszam int unsigned primary key,
nev varchar(30) not null,
anyja_neve varchar(30) not null,
varos varchar(25),
utca_hsz varchar(30),
szhely varchar(35),
szido date);

#b
alter table beteg add telszam varchar(12);
alter table beteg add gye char(1) default 'N' CHECK(gye = 'I' OR gye = 'N');

#c
alter table beteg modify varos varchar(30);

#d
alter table beteg rename to Beteg;

#e
#stb....

#f
update beteg set varos = 'Dunaújváros';

#g
alter table beteg drop utca_hsz;