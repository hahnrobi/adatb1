#a
create table konyv(
isbn char(13) primary key,
szerzo varchar(30) not null,
cim varchar(60),
kiado varchar(30),
ar int);

#b
alter table konyv modify cim varchar(100);

#c
alter table konyv change szerzo SZERZO varchar(30) not null;


#d
insert into konyv values(
'JI6JBEKDN712D','Ládi Csenge', 'A csótányok bosszúja', 'IVA Media', 1500);

insert into konyv values(
'ADWABEKDN712D','Szalai Márk', 'Fözünk mint az oroszok', 'Tamaskó Patrik', 12500);

insert into konyv values(
'1234BEKDN712D','Leonidász', 'A jó cigánypecsenye titka', 'Vakondtúrás', 4500);

insert into konyv values(
'JI6JBEKDN754A','Alekosz', 'Elmém titkai', 'Takarék', 5000);

insert into konyv values(
'JI6JBEDAS712D','Ferenc pápa', 'Jó vagyok?', 'Timy Trumpet', 8500);

#e nincs Kis István :(
update konyv set szerzo = 'Timmy Trumpet' where szerzo = 'Timy Trumpet';

#f Nincs tél sem :(
delete from konyv where cim LIKE '%cigány%';

#g
insert into konyv (szerzo, cim, kiado) values('Napsugár', 'Varga Anna', 'Libri');

#h
update konyv set kiado = 'Alexandra' where cim = 'Napsugár';