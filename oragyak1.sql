#a
create table dolgozok (
igszam char(8) primary key,
nev varchar(30) not null,
varos varchar(20),
utca_hsz varchar(30),
fizetes int unsigned,
szulido year,
neme char(1) default 'F',
check (neme = 'F' OR neme = 'N'));

#b
alter table dolgozok add szhely varchar(20);
alter table dolgozok add anyja_neve varchar(25);
alter table dolgozok add telszam varchar(12);

#c
alter table dolgozok modify varos varchar(30);

#d
alter table dolgozok modify fizetes int unsigned default 100000;

#e
alter table dolgozok rename to Dolgozok;

#f
insert into dolgozok values('12345678','Petofi Sándor', 'Nagydorog', 'Vasút u. 1.', 120000, '1811', 'F', 'Paks', 'Mária', '36201234567');
#f stb...

#g
update dolgozok set fizetes = 200000 where neme = 'N';

#h
update dolgozok set fizetes = fizetes + 50000 where neme = 'F';

#i
update dolgozok set fizetes = fizetes + 10000 where neme = 'F' AND fizetes < 180000;

#j
update dolgozok set fizetes = 150000 where varos LIKE 'D%';

#K
update dolgozok set fizetes = fizetes*1.15 where fizetes < 160000;